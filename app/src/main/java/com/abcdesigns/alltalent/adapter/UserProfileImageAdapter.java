package com.abcdesigns.alltalent.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.abcdesigns.alltalent.R;
import com.abcdesigns.alltalent.databinding.RowUserProfileImageBinding;
import com.abcdesigns.alltalent.model.UserImage;
import com.abcdesigns.alltalent.util.recycler_view.RecyclerViewProject;

import java.util.ArrayList;

public class UserProfileImageAdapter extends RecyclerView.Adapter<UserProfileImageAdapter.ViewHolder> {

    private Context context;
    private ArrayList<UserImage> arrayList;

    public UserProfileImageAdapter(Context context, ArrayList<UserImage> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RowUserProfileImageBinding binding = DataBindingUtil.inflate(inflater, R.layout.row_user_profile_image,parent,false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        UserImage userImage = arrayList.get(position);
        RecyclerViewProject.glideImplementation(context,userImage.getImageUrl(),userImage.getImageUrl(),holder.binding.progressBar,holder.binding.imageProfile);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        RowUserProfileImageBinding binding;

        public ViewHolder(@NonNull RowUserProfileImageBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }
    }

}
