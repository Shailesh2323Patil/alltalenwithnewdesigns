package com.abcdesigns.alltalent.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.abcdesigns.alltalent.R;
import com.abcdesigns.alltalent.databinding.RowTalentJobsBinding;
import com.abcdesigns.alltalent.databinding.RowUserProfileImageBinding;
import com.abcdesigns.alltalent.model.UserImage;
import com.abcdesigns.alltalent.util.recycler_view.RecyclerViewProject;

import java.util.ArrayList;

public class TalentJobsAdapter extends RecyclerView.Adapter<TalentJobsAdapter.ViewHolder> {

    private Context context;
    private ArrayList<UserImage> arrayList;

    public TalentJobsAdapter(Context context, ArrayList<UserImage> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RowTalentJobsBinding binding = DataBindingUtil.inflate(inflater, R.layout.row_talent_jobs,parent,false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        UserImage userImage = arrayList.get(position);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        RowTalentJobsBinding binding;

        public ViewHolder(@NonNull RowTalentJobsBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }
    }

}
