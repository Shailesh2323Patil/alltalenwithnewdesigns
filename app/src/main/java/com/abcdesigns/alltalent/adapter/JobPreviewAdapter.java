package com.abcdesigns.alltalent.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.abcdesigns.alltalent.ui.client.job_preview.AuditionDetailFragment;
import com.abcdesigns.alltalent.ui.client.job_preview.JobDetailFragment;


public class JobPreviewAdapter extends FragmentStateAdapter {
    public JobPreviewAdapter(@NonNull @org.jetbrains.annotations.NotNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }


    @NonNull
    @Override
    public Fragment createFragment(int position) {

        switch (position) {
            case 0:
                return new JobDetailFragment();
            case 1:
                return new AuditionDetailFragment();
            default:
                return new JobDetailFragment();
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}
