package com.abcdesigns.alltalent.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.abcdesigns.alltalent.R;
import com.abcdesigns.alltalent.databinding.RowProfileImageBinding;
import com.abcdesigns.alltalent.model.ProfileImage;
import com.abcdesigns.alltalent.util.recycler_view.RecyclerViewProject;

import java.util.ArrayList;


public class Adapter_Profile_Images extends RecyclerView.Adapter<Adapter_Profile_Images.ViewHolder>
{
    ArrayList<ProfileImage> arrayList;
    Context context;

    public Adapter_Profile_Images(Context context, ArrayList<ProfileImage> arrayList)
    {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RowProfileImageBinding view = DataBindingUtil.inflate(inflater, R.layout.row_profile_image, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position)
    {
        final ProfileImage profileImage = arrayList.get(position);

        RecyclerViewProject.glideImplementation(context,"",profileImage.getImage_path(),holder.binding.idProgressBar,holder.binding.idImg);

        holder.binding.idIconMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        holder.binding.idLayImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public int getItemCount()
    {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public RowProfileImageBinding binding;

        public ViewHolder(@NonNull RowProfileImageBinding view)
        {
            super(view.getRoot());
            this.binding = view;
        }
    }
}
