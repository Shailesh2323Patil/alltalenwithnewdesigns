package com.abcdesigns.alltalent.util.progress_bar;

public class ProgressItem {
    public int color;
    public float progressItemPercentage;
}
