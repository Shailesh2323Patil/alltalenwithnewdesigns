package com.abcdesigns.alltalent.util;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.abcdesigns.alltalent.R;
import com.google.android.material.snackbar.Snackbar;

public class PrintMessage
{
    public static void toastMsg(Context context, String message)
    {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void logE(String TAG, String message)
    {
        Log.e(TAG,message);
    }

    public static void logD(String TAG, String message)
    {
        Log.d(TAG,message);
    }

    public static void showErrorMessage(final Context context, String message, View view, final Class cls)
    {
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT)
                .setAction(context.getString(R.string.ok), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, cls);
                        context.startActivity(intent);
                    }
                })
                .show();
    }

    public static void showErrorMessage(Context context, String message, View view)
    {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
    }
}
