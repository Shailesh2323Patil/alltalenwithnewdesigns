package com.abcdesigns.alltalent.ui.user;

import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;

import com.abcdesigns.alltalent.BaseActivity;
import com.abcdesigns.alltalent.R;
import com.abcdesigns.alltalent.databinding.ActivityLoginBinding;


public class LoginActivity extends BaseActivity {

    ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        init();
    }

    private void init() {
        String dont_have_an_account = getString(R.string.dont_have_an_account);
        binding.txtSignUp.setText(Html.fromHtml(dont_have_an_account));


        binding.txtSignUp.setOnClickListener(this);
        binding.txtLoginWithEmail.setOnClickListener(this);
        binding.txtLoginWithOtp.setOnClickListener(this);
        binding.btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);

        switch (view.getId()) {
            case R.id.txt_sign_up : {
                Intent intent = new Intent(LoginActivity.this,SignupActivity.class);
                startActivity(intent);
            }
            break;

            case R.id.txt_login_with_email : {
                binding.layWithOtp.setVisibility(View.GONE);
                binding.layWithEmail.setVisibility(View.VISIBLE);
            }
            break;

            case R.id.txt_login_with_otp : {
                binding.layWithEmail.setVisibility(View.GONE);
                binding.layWithOtp.setVisibility(View.VISIBLE);
            }
            break;

            case R.id.btn_login : {
                Intent intent = new Intent(this,DashboardActivity.class);
                startActivity(intent);
                finish();
            }
            break;
        }
    }
}