package com.abcdesigns.alltalent.ui.user.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import com.abcdesigns.alltalent.R;
import com.abcdesigns.alltalent.adapter.ViewPagerAdapter;
import com.abcdesigns.alltalent.databinding.FragmentProfileBinding;
import com.abcdesigns.alltalent.util.progress_bar.ProgressItem;
import java.util.ArrayList;

public class ProfileFragment extends Fragment {

    public ProfileFragment() {
        // Required empty public constructor
    }

    FragmentProfileBinding binding;

    private ArrayList<ProgressItem> progressItemList;
    private ProgressItem mProgressItem;

    private int[] tabIcons = {
            R.drawable.icon_apps_sharp,
            R.drawable.icon_apps_sharp
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_profile, container, false);
        init();
        return binding.getRoot();
    }

    private void init() {
        binding.seekbarProfileStrength.getThumb().mutate().setAlpha(0);
        initDataToSeekbar();
        setViewPagerAdapter();
    }

    private void initDataToSeekbar() {
        progressItemList = new ArrayList<ProgressItem>();
        // red span
        mProgressItem = new ProgressItem();
        mProgressItem.progressItemPercentage = 20;
        mProgressItem.color = R.color.red;
        progressItemList.add(mProgressItem);
        // blue span
        mProgressItem = new ProgressItem();
        mProgressItem.progressItemPercentage = 25;
        mProgressItem.color = R.color.blue;
        progressItemList.add(mProgressItem);
        // green span
        mProgressItem = new ProgressItem();
        mProgressItem.progressItemPercentage = 35;
        mProgressItem.color = R.color.green;
        progressItemList.add(mProgressItem);

        //white span
        mProgressItem = new ProgressItem();
        mProgressItem.progressItemPercentage = 20;
        mProgressItem.color =  R.color.gray;
        progressItemList.add(mProgressItem);

        binding.seekbarProfileStrength.initData(progressItemList);
        binding.seekbarProfileStrength.invalidate();
    }

    private void setViewPagerAdapter() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new UserProfileImagesFragment(),"(20)");
        adapter.addFragment(new UserTalentJobListFragment(),"(05)");
        binding.viewPager.setAdapter(adapter);
        binding.tabLayout.setupWithViewPager(binding.viewPager);

        setupTabIcons();
    }

    private void setupTabIcons() {
        View inflatedView = getLayoutInflater().inflate(R.layout.shape_tab_layout,null);

        ImageView imageView = (ImageView) inflatedView.findViewById(R.id.tab_image);
        imageView.setImageDrawable(getResources().getDrawable(tabIcons[0]));
        TextView textView = (TextView) inflatedView.findViewById(R.id.text_content);
        textView.setText("(20)");

        binding.tabLayout.getTabAt(0).setCustomView(inflatedView);

        View inflatedView_1 = getLayoutInflater().inflate(R.layout.shape_tab_layout,null);

        ImageView imageView_1 = (ImageView) inflatedView_1.findViewById(R.id.tab_image);
        imageView_1.setImageDrawable(getResources().getDrawable(tabIcons[0]));
        TextView textView_1 = (TextView) inflatedView_1.findViewById(R.id.text_content);
        textView_1.setText("(20)");

        binding.tabLayout.getTabAt(1).setCustomView(inflatedView_1);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}