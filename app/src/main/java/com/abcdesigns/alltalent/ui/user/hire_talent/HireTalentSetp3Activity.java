package com.abcdesigns.alltalent.ui.user.hire_talent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.abcdesigns.alltalent.R;
import com.abcdesigns.alltalent.databinding.ActivityHireTalentSetp3Binding;

public class HireTalentSetp3Activity extends AppCompatActivity {

    ActivityHireTalentSetp3Binding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_hire_talent_setp_3);
    }
}