package com.abcdesigns.alltalent.ui.client.profile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.abcdesigns.alltalent.BaseActivity;
import com.abcdesigns.alltalent.R;
import com.abcdesigns.alltalent.databinding.ActivityClientProfile2Binding;

public class ClientProfile2Activity extends BaseActivity {

    ActivityClientProfile2Binding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_client_profile_2);

        init();
    }

    private void init() {
        binding.btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);

        switch (view.getId()) {
            case R.id.btn_next :
                    Intent intent = new Intent(this,ClientProfile3Activity.class);
                    startActivity(intent);
                break;
        }
    }
}