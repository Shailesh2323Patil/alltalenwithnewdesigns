package com.abcdesigns.alltalent.ui.client.job_preview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.abcdesigns.alltalent.R;
import com.abcdesigns.alltalent.databinding.ActivityJobPreviewBinding;

public class JobPreviewActivity extends AppCompatActivity {

    ActivityJobPreviewBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_job_preview);
    }
}