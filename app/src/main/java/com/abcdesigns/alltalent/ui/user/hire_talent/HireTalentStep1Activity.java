package com.abcdesigns.alltalent.ui.user.hire_talent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.abcdesigns.alltalent.BaseActivity;
import com.abcdesigns.alltalent.R;
import com.abcdesigns.alltalent.databinding.ActivityHireTalentStep1Binding;

public class HireTalentStep1Activity extends BaseActivity {

    ActivityHireTalentStep1Binding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_hire_talent_step_1);

        toolBar();
        init();
    }

    private void toolBar() {
        setSupportActionBar(binding.layToolbar.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icon_arrow_backward_white);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        binding.layToolbar.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void init() {
        binding.radioGrpParentChild.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                checkedId = binding.radioGrpParentChild.getCheckedRadioButtonId();

                switch (checkedId) {
                    case R.id.rad_my_self:
                        binding.layForMyChild.setVisibility(View.GONE);
                        binding.layForMySelf.setVisibility(View.VISIBLE);
                        break;

                    case R.id.rad_my_child:
                        binding.layForMySelf.setVisibility(View.GONE);
                        binding.layForMyChild.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });

        binding.radMySelf.setChecked(true);

        binding.btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);

        switch (view.getId()) {
            case R.id.btn_next:
                    Intent intent = new Intent(this, HireTalentStep1_1Activity.class);
                    startActivity(intent);
                break;
        }
    }
}