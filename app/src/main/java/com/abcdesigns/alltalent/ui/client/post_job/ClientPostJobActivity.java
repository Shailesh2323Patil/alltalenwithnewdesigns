package com.abcdesigns.alltalent.ui.client.post_job;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.abcdesigns.alltalent.R;
import com.abcdesigns.alltalent.databinding.ActivityClientPostJobBinding;

public class ClientPostJobActivity extends AppCompatActivity {

    ActivityClientPostJobBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_client_post_job);
    }
}