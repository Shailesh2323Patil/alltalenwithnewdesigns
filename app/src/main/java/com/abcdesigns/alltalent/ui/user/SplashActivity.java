package com.abcdesigns.alltalent.ui.user;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import androidx.databinding.DataBindingUtil;
import com.abcdesigns.alltalent.BaseActivity;
import com.abcdesigns.alltalent.R;
import com.abcdesigns.alltalent.databinding.ActivitySplashBinding;


public class SplashActivity extends BaseActivity {

    private static int SPLASH_SCREEN_TIME_OUT = 6000;
    ActivitySplashBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this,R.layout.activity_splash);

        if (Build.VERSION.SDK_INT >= 21)
        {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        changeStatusBarColor();

        playVideo();

        init();

        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run() {
                binding.videoView.stopPlayback();
                binding.layVideoView.setVisibility(View.GONE);
                binding.layFirstScreen.setVisibility(View.VISIBLE);
            }
        }, SPLASH_SCREEN_TIME_OUT);
    }

    private void init() {
        binding.btnLogin.setOnClickListener(this);
        binding.btnSignup.setOnClickListener(this);
    }

    private void changeStatusBarColor()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void playVideo() {
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(binding.videoView);
        binding.videoView.setMediaController(mediaController);
        binding.videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.introduction));
        binding.videoView.start();
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);

        switch (view.getId()) {

            case R.id.btn_login:

                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();

                break;

            case R.id.btn_signup:

                Intent intent2 = new Intent(this, SignupActivity.class);
                startActivity(intent2);
                finish();

                break;
        }
    }
}
