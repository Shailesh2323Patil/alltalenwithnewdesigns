package com.abcdesigns.alltalent.ui.client.profile;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import com.abcdesigns.alltalent.BaseActivity;
import com.abcdesigns.alltalent.R;
import com.abcdesigns.alltalent.databinding.ActivityClientProfile1Binding;


import java.util.ArrayList;

public class ClientProfile1Activity extends BaseActivity {

    ActivityClientProfile1Binding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_client_profile_1);

        init();
    }

    private void init() {
        ArrayList<String> arrayList = new ArrayList<String>();
        arrayList.add(getString(R.string.company));
        arrayList.add(getString(R.string.individual));

        ArrayAdapter arrayAdapter = new ArrayAdapter(this,R.layout.row_dropdown,arrayList);
        binding.autocompleteIAm.setAdapter(arrayAdapter);

        binding.autocompleteIAm.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedText = (String) parent.getItemAtPosition(position);

                switch (selectedText) {
                    case "Company" :
                            replaceFragment(new ClientProfileCompanyFragment(),false,"ClientProfileCompanyFragment");
                        break;

                    case "Individual" :
                            replaceFragment(new ClientProfileIndividualFragment(),false,"ClientProfileIndividualFragment");
                        break;
                }
             }
        });

        // Extra code For Set First Fragment
        binding.autocompleteIAm.setText(binding.autocompleteIAm.getAdapter().getItem(0).toString(), false);
        replaceFragment(new ClientProfileCompanyFragment(),false,"ClientProfileCompanyFragment");
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
    }

    public void replaceFragment(Fragment fragment, boolean addToBackStack, String tag) {
        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            if (addToBackStack) {
                fragmentTransaction.addToBackStack(tag);
            }
            fragmentTransaction.replace(R.id.lay_fragment, fragment).commitAllowingStateLoss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}