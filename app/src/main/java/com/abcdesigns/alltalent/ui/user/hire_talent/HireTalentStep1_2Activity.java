package com.abcdesigns.alltalent.ui.user.hire_talent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.abcdesigns.alltalent.BaseActivity;
import com.abcdesigns.alltalent.R;
import com.abcdesigns.alltalent.databinding.ActivityHireTalentStep12Binding;
import com.abcdesigns.alltalent.databinding.ActivityHireTalentStep1Binding;

public class HireTalentStep1_2Activity extends BaseActivity {

    ActivityHireTalentStep12Binding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_hire_talent_step1_2);

        init();
    }

    private void init() {
        binding.btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);

        switch (view.getId()) {
            case R.id.btn_next:
                Intent intent = new Intent(this, HireTalentSetp3Activity.class);
                startActivity(intent);
                break;
        }
    }
}