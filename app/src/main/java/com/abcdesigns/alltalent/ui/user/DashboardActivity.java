package com.abcdesigns.alltalent.ui.user;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;

import com.abcdesigns.alltalent.R;
import com.abcdesigns.alltalent.databinding.ActivityDashboardBinding;
import com.abcdesigns.alltalent.ui.user.profile.ProfileFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class DashboardActivity extends AppCompatActivity {

    ActivityDashboardBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard);

        bottomNavigation();
    }

    private void bottomNavigation() {
        BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener =
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                        Fragment fragment;

                        switch (item.getItemId()) {
                            case R.id.bottom_navigation_jobs : {

                            }
                            return true;

                            case R.id.bottom_navigation_services : {

                            }
                            return true;

                            case R.id.bottom_navigation_user_dashboard : {
                                fragment = new UserDashboardFragment();
                                loadFragment(fragment,true,getString(R.string.user_dashboard_fragment));
                            }
                            return true;

                            case R.id.bottom_navigation_calendar : {

                            }
                            return true;

                            case R.id.bottom_navigation_profile : {
                                fragment = new ProfileFragment();
                                loadFragment(fragment,true,getString(R.string.profile_fragment));
                            }
                            return true;
                        }

                        return false;
                    }
                };

        binding.bottomNavigation.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);
        binding.bottomNavigation.setSelectedItemId(R.id.bottom_navigation_user_dashboard);
    }

    private void loadFragment(Fragment fragment,Boolean addToBackStack,String fragmentName) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(binding.layContainer.getId(),fragment);
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(fragmentName);
        }
        fragmentTransaction.commit();
    }
}