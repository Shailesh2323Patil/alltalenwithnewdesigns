package com.abcdesigns.alltalent.ui.client.profile;

import android.content.Intent;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.abcdesigns.alltalent.R;
import com.abcdesigns.alltalent.databinding.FragmentClientProfileCompanyBinding;

public class ClientProfileCompanyFragment extends Fragment implements View.OnClickListener {

    FragmentClientProfileCompanyBinding binding;

    public ClientProfileCompanyFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_client_profile_company, container, false);

        init();

        return binding.getRoot();
    }

    private void init() {
        binding.btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_next :
                Intent intent = new Intent(getActivity(),ClientProfile2Activity.class);
                startActivity(intent);
            break;
        }

    }
}