package com.abcdesigns.alltalent.ui.user;

import android.content.Intent;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.abcdesigns.alltalent.R;
import com.abcdesigns.alltalent.databinding.FragmentUserDashboardBinding;
import com.abcdesigns.alltalent.ui.client.profile.ClientProfile1Activity;
import com.abcdesigns.alltalent.ui.user.hire_talent.HireTalentStep1Activity;

public class UserDashboardFragment extends Fragment implements View.OnClickListener{

    FragmentUserDashboardBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_user_dashboard, container, false);
        init();
        return binding.getRoot();
    }

    private void init() {
        binding.cardFindWorkAsTalent.setOnClickListener(this);
        binding.cardHireTalentForYourProjects.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.card_find_work_as_talent :
                    Intent intent = new Intent(getActivity(), HireTalentStep1Activity.class);
                    startActivity(intent);
                break;

            case R.id.card_hire_talent_for_your_projects :
                    Intent intent_1 = new Intent(getActivity(), ClientProfile1Activity.class);
                    startActivity(intent_1);
                break;
        }
    }
}