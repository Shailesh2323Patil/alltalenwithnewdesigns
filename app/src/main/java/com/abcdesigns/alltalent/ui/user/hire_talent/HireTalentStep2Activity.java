package com.abcdesigns.alltalent.ui.user.hire_talent;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.abcdesigns.alltalent.BaseActivity;
import com.abcdesigns.alltalent.R;
import com.abcdesigns.alltalent.adapter.Adapter_Profile_Images;
import com.abcdesigns.alltalent.databinding.ActivityHireTalentStep2Binding;
import com.abcdesigns.alltalent.model.ProfileImage;
import com.abcdesigns.alltalent.util.PrintMessage;
import com.abcdesigns.alltalent.util.camera.CameraUtils;
import com.abcdesigns.alltalent.util.camera.FileUtils;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.ArrayList;
import java.util.List;

public class HireTalentStep2Activity extends BaseActivity {

    private static final int SELECT_PICTURES = 121;
    ActivityHireTalentStep2Binding binding;
    ArrayList<ProfileImage> arrayListImages;
    Adapter_Profile_Images adapter_profile_images;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_hire_talent_step_2);

        init();
        setRecyclerView();
    }

    private void init() {
        binding.txtAddMore.setOnClickListener(this);
        binding.imageAddImage.setOnClickListener(this);
    }

    private void setRecyclerView() {
        arrayListImages = new ArrayList<ProfileImage>();

        adapter_profile_images = new Adapter_Profile_Images(this, arrayListImages);
        RecyclerView.LayoutManager manager = new GridLayoutManager(this, 3);
        binding.recycleImages.setLayoutManager(manager);
        binding.recycleImages.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        binding.recycleImages.setAdapter(adapter_profile_images);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);

        switch (view.getId()) {

            case R.id.txt_add_more :
                requestCameraPermission();
                break;

            case R.id.image_add_image :
                requestCameraPermission();
                break;

        }
    }

    private void requestCameraPermission() {
        Dexter.withActivity(HireTalentStep2Activity.this)
                .withPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            selectPhotos();
                        } else if (report.isAnyPermissionPermanentlyDenied()) {
                            showPermissionsAlert();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void showPermissionsAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.permission_required))
                .setMessage(R.string.permission_camera_message)
                .setPositiveButton(getString(R.string.go_to_settings), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        CameraUtils.openSettings(HireTalentStep2Activity.this);
                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void selectPhotos() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        //allows any image file type. Change * to specific extension to limit it
        //**The following line is the important one!
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_images)), SELECT_PICTURES);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_PICTURES) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.getClipData() != null) {
                    try {
                        //evaluate the count before the for loop --- otherwise, the count is evaluated every loop.
                        int count = data.getClipData().getItemCount();

                        for (int i = 0; i < count; i++) {
                            Uri imageUri = data.getClipData().getItemAt(i).getUri();

                            ProfileImage profileImage = new ProfileImage();
                            profileImage.setImage_path(FileUtils.getPath(HireTalentStep2Activity.this, imageUri));
                            profileImage.setIs_profile_image("0");

                            arrayListImages.add(0, profileImage);
                        }

                        binding.layAddFirstAddImages.setVisibility(View.GONE);
                        binding.layShowAllImages.setVisibility(View.VISIBLE);
                    } catch (Exception e) {
                        e.printStackTrace();
                        PrintMessage.showErrorMessage(HireTalentStep2Activity.this, getString(R.string.gallery_not_working), binding.layRoot);
                    }
                }
                adapter_profile_images.notifyDataSetChanged();
            } else if (resultCode == Activity.RESULT_CANCELED) {
                PrintMessage.showErrorMessage(HireTalentStep2Activity.this, getString(R.string.no_image_selected), binding.layRoot);
            } else {
                PrintMessage.showErrorMessage(HireTalentStep2Activity.this, getString(R.string.no_image_selected), binding.layRoot);
            }
        }
    }
}