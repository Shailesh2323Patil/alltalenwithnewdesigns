package com.abcdesigns.alltalent.ui.user.hire_talent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.abcdesigns.alltalent.BaseActivity;
import com.abcdesigns.alltalent.R;
import com.abcdesigns.alltalent.databinding.ActivityHireTalentStep11Binding;

public class HireTalentStep1_1Activity extends BaseActivity {

    ActivityHireTalentStep11Binding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_hire_talent_step1_1);

        init();
    }

    private void init() {
        binding.btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);

        switch (view.getId()) {
            case R.id.btn_next:
                Intent intent = new Intent(this, HireTalentStep1_2Activity.class);
                startActivity(intent);
                break;
        }
    }
}