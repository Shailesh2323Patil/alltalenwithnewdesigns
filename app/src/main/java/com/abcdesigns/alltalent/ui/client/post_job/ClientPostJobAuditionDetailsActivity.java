package com.abcdesigns.alltalent.ui.client.post_job;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.abcdesigns.alltalent.R;

public class ClientPostJobAuditionDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_post_job_audition_details);
    }
}