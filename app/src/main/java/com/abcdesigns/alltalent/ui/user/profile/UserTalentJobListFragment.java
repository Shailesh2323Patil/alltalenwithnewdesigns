package com.abcdesigns.alltalent.ui.user.profile;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.abcdesigns.alltalent.R;
import com.abcdesigns.alltalent.adapter.TalentJobsAdapter;
import com.abcdesigns.alltalent.adapter.UserProfileImageAdapter;
import com.abcdesigns.alltalent.databinding.FragmentUserTalentJobListBinding;
import com.abcdesigns.alltalent.model.UserImage;

import java.util.ArrayList;

public class UserTalentJobListFragment extends Fragment {

    FragmentUserTalentJobListBinding binding;
    TalentJobsAdapter adapter;

    public UserTalentJobListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_user_talent_job_list, container, false);
        init();
        return binding.getRoot();
    }

    private void init() {
        UserImage userImage = new UserImage();
        userImage.setImageUrl("https://alltalents.in/img/professional_categories/model.jpg");

        ArrayList<UserImage> arrayList = new ArrayList<UserImage>();

        for (int i = 0; i < 20; i++) {
            arrayList.add(userImage);
        }

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),1, RecyclerView.VERTICAL,false);
        adapter = new TalentJobsAdapter(getActivity(),arrayList);
        binding.recyclerTalentJobs.setLayoutManager(gridLayoutManager);
        binding.recyclerTalentJobs.setAdapter(adapter);
    }
}