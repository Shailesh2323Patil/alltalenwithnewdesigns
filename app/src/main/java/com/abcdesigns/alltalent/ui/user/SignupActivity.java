package com.abcdesigns.alltalent.ui.user;

import android.os.Bundle;

import com.abcdesigns.alltalent.BaseActivity;
import com.abcdesigns.alltalent.R;

public class SignupActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
    }
}